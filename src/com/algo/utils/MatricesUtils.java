package com.algo.utils;

import java.util.ArrayList;
import java.util.List;

import com.algo.entities.Matrix;

@SuppressWarnings("unused")
public class MatricesUtils {

	public static Integer[][] insertIntoMatrix(List<List<Integer>> nestedMatrixRows, Integer rows, Integer columns) {
		Integer r = 0;
		Integer c = 0;
		Integer[][] matrix = new Integer[rows][columns];

		for (List<Integer> matrixRow : nestedMatrixRows) {
			for (Integer entry : matrixRow) {
				matrix[r][c] = entry;
				c++;
				// Validation Check to avoid arrayoutofboundindex exception
				if (c > columns)
					break;
			}
			r++;
			c = 0;
			if (r > rows)
				break;

		}
		return matrix;
	}

	public static Integer[][] multiplyNSquareMatrix(List<Integer[][]> matrices) {
		Integer[][] firstMatrix = matrices.stream().findFirst().get();
		Integer[][] resultMatrix = new Integer[firstMatrix.length][firstMatrix[0].length];
		for (Integer[][] matrix : matrices) {
			resultMatrix = addTwoMatrix(matrix, resultMatrix);
		}
		return resultMatrix;
	}

	public static Integer[][] addTwoMatrix(Integer[][] matrix, Integer[][] resultMatrix) {
		Integer[][] tempMatrix = new Integer[matrix.length][matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				int sum = 0;
				if (resultMatrix[i][j] != null) {
					sum = 0;
					for (int k = 0; k < matrix[0].length; k++) {
						sum += resultMatrix[i][k] * matrix[k][i];
					}
					tempMatrix[i][j] = sum;
				} else
					tempMatrix[i][j] = matrix[i][j];

			}
		}

		return tempMatrix;
	}

	private static Integer[][] copyAMatrix(Integer[][] tempMatrix) {
		Integer[][] copiedMatrix = new Integer[tempMatrix.length][tempMatrix[0].length];
		for (int i = 0; i < tempMatrix.length; i++) {
			for (int j = 0; j < tempMatrix[0].length; j++) {
				copiedMatrix[i][j] = tempMatrix[i][j];
			}
		}
		return copiedMatrix;
	}

	public static List<List<Integer>> mockRowsOfRRowsAndCColumns(Integer rows, Integer columns) {
		List<List<Integer>> matRow = new ArrayList<List<Integer>>();
		for (int i = 0; i < rows; i++) {
			List<Integer> row = new ArrayList<>();
			for (int j = 0; j < columns; j++) {
				row.add(randomInt());
			}
			matRow.add(row);
		}
		return matRow;
	}

	public static void printAMatrix(Integer[][] matrix) {
		System.out.println("------Matrix Data------");

		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[");
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.print("]");
			System.out.println();
		}

	}

	public static int randomInt() {
		return (int) (Math.random() * 10);
	}

	public static List<List<Integer>> matrixToNestedList(Matrix matrix) {
		List<List<Integer>> matRows = new ArrayList<List<Integer>>();
		Integer[][] mat = matrix.getMatrix();
		for (int i = 0; i < matrix.getRows(); i++) {
			List<Integer> row = new ArrayList<>();
			for (int j = 0; j < matrix.getColumns(); j++) {
				row.add(mat[i][j]);
			}
			matRows.add(row);
		}
		return matRows;
	}

	public static Integer[][] identityMatrix(Integer rows, Integer columns) {
		Integer identityFillPositionRowAndColumn = 0;
		Integer[][] matrix = new Integer[rows][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (!(i == identityFillPositionRowAndColumn && j == identityFillPositionRowAndColumn)) {
					matrix[i][j] = 0;
				} else {
					matrix[i][j] = 1;
					identityFillPositionRowAndColumn++;
				}
			}
		}
		return matrix;
	}

	public static Integer[][] singleValueMatrix(Integer rows, Integer columns, Integer numberToAdd) {
		Integer[][] matrix = new Integer[rows][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				matrix[i][j] = numberToAdd;
			}
		}
		return matrix;
	}
}
