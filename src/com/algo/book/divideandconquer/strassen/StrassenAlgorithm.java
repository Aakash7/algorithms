package com.algo.book.divideandconquer.strassen;

/*
 * The algorithm is designed to consume less amount of time as compare 
 * to conventional method and should be ideally used when you have Square Matrices and the rows = columns > 256
 * 
 * */

public class StrassenAlgorithm {

	public static int[][] multiplySquareMatrices(int[][] firstMatrix, int[][] secondMatrix) {

		int length = firstMatrix.length;
		int[][] resultMatrix = new int[length][length];

		if (length == 1)
			resultMatrix[0][0] = firstMatrix[0][0] * secondMatrix[0][0];
		else {
			int[][] A11 = new int[length / 2][length / 2];
			int[][] A12 = new int[length / 2][length / 2];
			int[][] A21 = new int[length / 2][length / 2];
			int[][] A22 = new int[length / 2][length / 2];

			int[][] B11 = new int[length / 2][length / 2];
			int[][] B12 = new int[length / 2][length / 2];
			int[][] B21 = new int[length / 2][length / 2];
			int[][] B22 = new int[length / 2][length / 2];

			/*
			 * Split a square matrix such that each cell of a matrix is stored in a single
			 * length matrix
			 */
			splitMatrix(firstMatrix, A11, 0, 0);
			splitMatrix(firstMatrix, A12, 0, length / 2);
			splitMatrix(firstMatrix, A21, length / 2, 0);
			splitMatrix(firstMatrix, A22, length / 2, length / 2);

			splitMatrix(secondMatrix, B11, 0, 0);
			splitMatrix(secondMatrix, B12, 0, length / 2);
			splitMatrix(secondMatrix, B21, length / 2, 0);
			splitMatrix(secondMatrix, B22, length / 2, length / 2);

			int[][] M1 = multiplySquareMatrices(addTwoMatrix(A11, A22), addTwoMatrix(B11, B22));
			int[][] M2 = multiplySquareMatrices(addTwoMatrix(A21, A22), B11);
			int[][] M3 = multiplySquareMatrices(A11, subTwoMatrix(B12, B22));
			int[][] M4 = multiplySquareMatrices(A22, subTwoMatrix(B21, B11));
			int[][] M5 = multiplySquareMatrices(addTwoMatrix(A11, A12), B22);
			int[][] M6 = multiplySquareMatrices(subTwoMatrix(A21, A11), addTwoMatrix(B11, B12));
			int[][] M7 = multiplySquareMatrices(subTwoMatrix(A12, A22), addTwoMatrix(B21, B22));

			int[][] C11 = addTwoMatrix(subTwoMatrix(addTwoMatrix(M1, M4), M5), M7);
			int[][] C12 = addTwoMatrix(M3, M5);
			int[][] C21 = addTwoMatrix(M2, M4);
			int[][] C22 = addTwoMatrix(subTwoMatrix(addTwoMatrix(M1, M3), M2), M6);

			/** join 4 halves into one result matrix **/
			joinMatrix(C11, resultMatrix, 0, 0);
			joinMatrix(C12, resultMatrix, 0, length / 2);
			joinMatrix(C21, resultMatrix, length / 2, 0);
			joinMatrix(C22, resultMatrix, length / 2, length / 2);

		}
		/** return result **/
		return resultMatrix;

	}

	public static void splitMatrix(int[][] matrix, int[][] resultMatrix, int rows, int columns) {
		for (int i = 0, i2 = rows; i < resultMatrix.length; i++, i2++) {
			for (int j = 0, j2 = columns; j < resultMatrix.length; j++, j2++) {
				resultMatrix[i][j] = matrix[i2][j2];
			}
		}
	}

	public static void joinMatrix(int[][] firstMatrix, int[][] secondMatrix, int rows, int columns) {
		for (int i = 0, i2 = rows; i < firstMatrix.length; i++, i2++) {
			for (int j = 0, j2 = columns; j < firstMatrix.length; j++, j2++) {
				secondMatrix[i2][j2] = firstMatrix[i][j];
			}
		}
	}

	public static int[][] addTwoMatrix(int[][] firstMatrix, int[][] secondMatrix) {
		int[][] tempMatrix = new int[firstMatrix.length][firstMatrix.length];
		for (int i = 0; i < firstMatrix.length; i++) {
			for (int j = 0; j < firstMatrix.length; j++) {
				tempMatrix[i][j] = secondMatrix[i][j] + firstMatrix[i][j];
			}
		}
		return tempMatrix;
	}

	public static int[][] subTwoMatrix(int[][] firstMatrix, int[][] secondMatrix) {
		int[][] tempMatrix = new int[firstMatrix.length][firstMatrix.length];
		for (int i = 0; i < firstMatrix.length; i++) {
			for (int j = 0; j < firstMatrix.length; j++) {
				tempMatrix[i][j] = firstMatrix[i][j] - secondMatrix[i][j];
			}
		}
		return tempMatrix;
	}

	public static int[][] singleValueMatrix(Integer rows, Integer columns, Integer numberToAdd) {
		int[][] matrix = new int[rows][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				matrix[i][j] = numberToAdd;
			}
		}
		return matrix;
	}

	public static void printAMatrix(int[][] matrix) {
		System.out.println("------Matrix Data------");

		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[");
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.print("]");
			System.out.println();
		}

	}
}
