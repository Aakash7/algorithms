package com.algo.book.divideandconquer;

import static com.algo.utils.MatricesUtils.insertIntoMatrix;
import static com.algo.utils.MatricesUtils.mockRowsOfRRowsAndCColumns;
import static com.algo.utils.MatricesUtils.multiplyNSquareMatrix;
import static com.algo.utils.MatricesUtils.printAMatrix;
import static com.algo.utils.MatricesUtils.singleValueMatrix;

import java.util.Arrays;

import com.algo.book.divideandconquer.strassen.StrassenAlgorithm;

/*
 * Change the conventional method to be 
 * flexible for non-squared matrix along with validations
 * 
 * */

public class MultiplyTwoMatrices {
	static Integer columns;
	static Integer rows = columns = 4;

	public static void main(String[] args) {
		Integer[][] firstMatrix = insertIntoMatrix(mockRowsOfRRowsAndCColumns(rows, columns), rows, columns);

		System.out.println("-----Without Strassen Algorithm-----");

		printAMatrix(multiplyNSquareMatrix(Arrays.asList(firstMatrix, singleValueMatrix(rows, columns, 1))));

		System.out.println("-----With Strassen Algorithm-----");

		StrassenAlgorithm.printAMatrix(
				StrassenAlgorithm.multiplySquareMatrices(StrassenAlgorithm.singleValueMatrix(rows, columns, 1),
						StrassenAlgorithm.singleValueMatrix(rows, columns, 1)));

		// StrassenAlgorithm.printAMatrix(Strassen.multiply(StrassenAlgorithm.singleValueMatrix(rows,
		// columns, 1), StrassenAlgorithm.singleValueMatrix(rows, columns, 1)));

	}

}
