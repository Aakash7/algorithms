package com.algo.book.divideandconquer;

import java.util.HashMap;

/*Divide and Conquer Algorithm - MaximumSubArray code*/

/*Find Max Sub Array for given points
such that sum of x numbers of array 
gives maximum positive result*/

public class MaxSubArray {
	static final String HIGH = "high";
	static final String LOW = "low";
	static final String SUM = "sum";

	public static void main(String args[]) {
		int[] input = { -10, -5, -6, -20, -10, -15, -20, -6, -15, -10 };

		HashMap<String, Integer> map = maxSubArray(input, 0, input.length - 1);
		System.out
				.println(SUM + " " + map.get(SUM) + " " + LOW + " " + map.get(LOW) + " " + HIGH + " " + map.get(HIGH));
	}

	static HashMap<String, Integer> maxSubArray(int[] input, int low, int high) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		if (high == low) {
			map.put(HIGH, high);
			map.put(LOW, low);
			map.put(SUM, input[low]);
			return map;
		} else {
			int mid = (int) (low + high) / 2;
			HashMap<String, Integer> leftSubArrayMap;
			HashMap<String, Integer> rightSubArrayMap;
			HashMap<String, Integer> crossSubArrayMap;
			try {
				/*
				 * //Divide the array to it smallest point, compare the leftSubArrayMap w/
				 * rightSubArrayMap & crossSubArrayMap, the array which calculates the max sum,
				 * should be returned
				 */
				leftSubArrayMap = maxSubArray(input, low, mid);
				rightSubArrayMap = maxSubArray(input, mid + 1, high);
				crossSubArrayMap = maxCrossingSubArray(input, low, high);

				if (leftSubArrayMap.get(SUM) > rightSubArrayMap.get(SUM)
						&& leftSubArrayMap.get(SUM) > crossSubArrayMap.get(SUM))
					return leftSubArrayMap;
				else if (rightSubArrayMap.get(SUM) > leftSubArrayMap.get(SUM)
						&& rightSubArrayMap.get(SUM) > crossSubArrayMap.get(SUM))
					return rightSubArrayMap;
				else
					return crossSubArrayMap;
			} catch (Exception e) {
				System.out.println(low + " " + high + " " + mid);
				e.printStackTrace();
				throw e;
			}
		}
	}

	// cross-sub-array algo
	// It calculates the max sum between the two divided sub array
	static HashMap<String, Integer> maxCrossingSubArray(int[] input, int low, int high) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		Integer leftSum = null;
		Integer rightSum = null;
		Integer sum = 0;
		int mid = (low + high) / 2;
		try {
			// If sum < input + sum then replace leftSum with the r.h.s
			for (int i = mid; i >= low; i--) {
				sum = sum + input[i];
				if (leftSum == null || leftSum < sum) {
					leftSum = sum;
				}
			}
			sum = 0;
			// If sum < input + sum then replace rightSum with the r.h.s
			for (int j = mid + 1; j <= high; j++) {
				sum = sum + input[j];
				if (rightSum == null || rightSum < sum) {
					rightSum = sum;
				}
			}
			map.put(HIGH, high);
			map.put(LOW, low);
			map.put(SUM, leftSum + rightSum);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(low + " " + high + " " + mid);
			e.printStackTrace();
			throw e;
		}
		return map;

	}
}
