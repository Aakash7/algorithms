package com.algo.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Matrix {
	
	private Integer rows;
	
	private Integer columns;
	
	//List<HashMap<Integer, Integer>> matriceRow = new LinkedList<HashMap<Integer,Integer>>();
	
	private List<ArrayList<Integer>> matriceRows = new ArrayList<ArrayList<Integer>>();
	
	private Integer[][] matrix;

	public Matrix(Integer rows, Integer columns) {
		this.rows = rows;
		this.columns = columns;
		matrix = new Integer [rows] [columns];
	}
	

	public Matrix(Integer rows, Integer columns, List<ArrayList<Integer>> matriceRows) {
		this.rows = rows;
		this.columns = columns;
		this.matriceRows = matriceRows;
	}

	
	

	public Matrix(Integer rows, Integer columns, Integer[][] matrix) {
		super();
		this.rows = rows;
		this.columns = columns;
		this.matrix = matrix;
	}


	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public Integer getColumns() {
		return columns;
	}

	public void setColumns(Integer columns) {
		this.columns = columns;
	}

	public List<ArrayList<Integer>> getMatriceRows() {
		return matriceRows;
	}

	public void setMatriceRows(List<ArrayList<Integer>> matriceRows) {
		this.matriceRows = matriceRows;
	}


	public Integer[][] getMatrix() {
		return matrix;
	}


	public void setMatrix(Integer[][] matrix) {
		this.matrix = matrix;
	}

	

	@Override
	public String toString() {
		return "Matrix [matrix=" + Arrays.toString(matrix) + "]";
	}
	
	
	
	
	
}
